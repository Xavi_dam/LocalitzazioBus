package com.example.payasu.localbus;

import java.text.DecimalFormat;

/**
 * Created by payasu on 22/04/18.
 */

public class Localitzacio {

    public double latitud;
    public double longitud;
    public long tiempo;

    public Localitzacio()
    {

    }
    public Localitzacio(double latitud, double longitud, long tiempo) {
        this.latitud = latitud;
        this.longitud = longitud;
        this.tiempo = tiempo;
    }

    public static String formatear (double magnitud) {
        DecimalFormat nf = new DecimalFormat("#.0000");
        return String.valueOf(nf.format(magnitud));
    }
}
